CREATE TABLE "user" (
    id SERIAL PRIMARY KEY,
    "name" text NOT NULL,
    "passwordHash" text NOT NULL,
    "expirationTime" double precision
);

CREATE TABLE "token" (
    id SERIAL PRIMARY KEY,
    token text NOT NULL,
    account integer
);

CREATE TABLE "subscription" (
    id SERIAL PRIMARY KEY,
    code text NOT NULL,
    days integer NOT NULL,
    "generatedBy" integer,
    activated boolean DEFAULT FALSE
);
