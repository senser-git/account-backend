import { Request, Response, NextFunction } from "express";
import { Token } from "@database/Token";

export default async (req: Request, res: Response, next: NextFunction) => {
    const { headers } = req;
    const user = await Token.verify(headers.authorization || "");

    if (!user) return res.status(401).json({
        success: false,
        message: "Unauthorized"
    });

    req.user = user;
    next();
};