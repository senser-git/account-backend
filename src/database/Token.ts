import { Column, Entity, PrimaryGeneratedColumn, BaseEntity } from "typeorm";
import { randomBytes } from "crypto";
import { User } from "./User";

@Entity()
export class Token extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    token: string;
  
    @Column()
    account: number;

    static async generateToken(userId: number) {
        const accessToken = new this();

        accessToken.token = randomBytes(100).toString("base64");
        accessToken.account = userId;

        await accessToken.save();

        return accessToken;
    };

    static async verify(token: string) {
        const data = await this.findOne({ token });
        if (!data) return false;

        const user = await User.findOne(data.account);
        if(!user) return false;

        return user;
    };
};