import { Column, Entity, PrimaryGeneratedColumn, BaseEntity } from "typeorm";
import { hash, verify } from "argon2";
import { Token } from "./Token";
import { Subscription } from "./Subscription";

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    passwordHash: string;

    @Column({ nullable: true })
    expirationTime: number;

    static async register(name: string, pw: string) {
        if (await this.findOne({ name })) return false;

        const user = new this();
        user.name = name;
        user.passwordHash = await hash(pw);
        return await user.save();
    };

    static async login(name: string, pw: string) {
        const user = await this.findOne({ name });
        if (!user) return false;

        if (!verify(user.passwordHash, pw)) return false;

        const { token } = await Token.generateToken(user.id);

        return token;
    };

    updateExpirationDays(days) {
        const date = new Date();
        const epoch = date.getTime() / 1000;
        
        if (!this.expirationTime) this.expirationTime = epoch;

        // else will extend the time
        if (epoch >= this.expirationTime) {
            date.setDate(date.getDate() + days);  
            this.expirationTime = date.getTime() / 1000;
        } else {
            const currentDate = new Date(this.expirationTime * 1000);
            currentDate.setDate(currentDate.getDate() + days); 

            this.expirationTime = currentDate.getTime() / 1000;
        };
    };

    isNotExpired() {
        if (this.expirationTime === 0) return true;
        const date = new Date();
        const epoch = date.getTime() / 1000;

        return epoch < this.expirationTime;
    };

    hasSub() {
        if (!this.expirationTime) return false;

        return this.isNotExpired();
    };

    isEnterprise() {
        return this.hasSub() && this.expirationTime === 0 &&
            true || false
    };

    toObject() {
        const data = { 
            ...this,
            hasSubscription: this.hasSub()
        };

        delete data.passwordHash;
        delete data.expirationTime;

        return data;
    };

    async redeem(code: string) {
        const sub = await Subscription.findOne({ code });

        if (!sub || sub.activated) return false;
        if (this.expirationTime && this.expirationTime === 0) return false;

        if (sub.days === -1) // is lifetime
            this.expirationTime = 0;
        else
            this.updateExpirationDays(sub.days);
        
        sub.activated = true;

        await Promise.all([
            sub.save(),
            this.save()
        ]);

        return true;
    };
};