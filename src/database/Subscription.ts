import { Column, Entity, PrimaryGeneratedColumn, BaseEntity } from "typeorm";
import { v4 } from "uuid";

@Entity()
export class Subscription extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @Column()
    days: number; // -1 = lifetime

    @Column()
    generatedBy: number;

    @Column({
        default: false
    })
    activated: boolean;

    static async generate(days: number, creator?: number) {
        const sub = new this();

        sub.code = v4();
        sub.days = days;
        if (creator) sub.generatedBy = creator;

        return await sub.save();
    };
};