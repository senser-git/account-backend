import 'reflect-metadata';
import 'module-alias/register';

import { config } from "dotenv";
import express from "express";
import AccountRouter from "@routes/Account";
import AdminRouter from "@routes/Admin";
import { createConnection } from "typeorm";
import { User } from "@database/User";
import { Token } from "@database/Token";
import { Subscription } from "@database/Subscription";

if (process.env.NODE_ENV !== "production") config();

const app = express();

/*app.get("*", (req, res) => res.status(404).json({
    success: false,
    message: "Invaild Route"
}));*/

app.use("/account", AccountRouter);
app.use("/admin", AdminRouter);

createConnection({
    type: "postgres",
    username: process.env.PG_USERNAME,
    password: process.env.PG_PASSWORD,
    database: process.env.PG_DB,
    host: process.env.PG_HOST,
    entities: [ User, Token, Subscription ],
}).then(async () => {
    app.listen(process.env.PORT, () => console.log("HTTP is up"));
});