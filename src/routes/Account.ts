import { Router, json } from "express";
import { User } from "@database/User";
import AuthMiddleware from "@components/middlewares/Auth";

const router = Router();

router.use(json());

router.get("/@me", AuthMiddleware, async (req, res) => {
    res.json(req.user.toObject());
});

router.post("/register", async (req, res) => {
    const { body } = req;

    if (!body.name || !body.password) return res.status(400).json({
        success: false,
        message: "Username or password is missing"
    });

    const user = await User.register(body.name, body.password);

    if (!user) return res.status(409).json({
        success: false,
        message: "The username has been already registered"
    });
    
    res.json({
        success: true,
        user: user.toObject()
    });
});

router.post("/login", async (req, res) => {
    const { body } = req;

    if (!body.name || !body.password) return res.status(400).json({
        success: false,
        message: "Username or password is missing"
    });

    const token = await User.login(body.name, body.password);

    if (!token) return res.status(404).json({
        success: false,
        message: "The user credentials are invaild"
    });


    res.json({
        success: true,
        token
    });
});

router.post("/@me/redeem", AuthMiddleware, async (req, res) => {
    const { body } = req;

    if (!body.key) return res.status(400).json({
        success: false,
        message: "Key is missing"
    });

    const response = await req.user.redeem(body.key);

    if (!response) return res.status(400).json({
        success: false,
        message: "The key is activated or invaild or you have lifetime"
    });
    
    res.json({
        success: true,
        message: "Your sub should be activated"
    });
});

export default router;