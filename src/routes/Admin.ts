import { Router, json } from "express";
import { Subscription } from "@database/Subscription";

const router = Router();

router.use(json());

router.post("/sub/generate", async (req, res) => {
    const sub = await Subscription.generate(1);

    res.json({
        success: true,
        code: sub.code
    });
});

export default router;