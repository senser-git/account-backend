import { User } from "../src/database/User";

declare global {
    namespace Express {
        interface Request { 
            user: User;
        };
    };
};
